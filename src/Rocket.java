import utils.Utils;

public class Rocket implements  SpaceShip{

    int maxWeight;
    int currentWeith ;

    public Rocket(int maxWeight, int currentWeith){
        this.maxWeight = maxWeight;
        this.currentWeith = currentWeith;
    }

    @Override
    public boolean launch() {
        return true;
    }

    @Override
    public boolean land() {
        return true;
    }

    @Override
    public boolean canCarry(Item item) {
        if (currentWeith + Utils.getTonsByKilograms(item.getWeight()) <= maxWeight){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void carry(Item item) {
        currentWeith += Utils.getTonsByKilograms(item.getWeight());
    }

}
