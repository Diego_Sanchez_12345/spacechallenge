package utils;

public class Utils {

    public static void showMessage(String msg){
        System.out.println(msg);
    }

    public static double getTonsByKilograms(int kilograms){
        return kilograms / 907.185;
    }
}
