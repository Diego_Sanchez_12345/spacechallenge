import utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Simulation {

    public ArrayList<Item> loadItems(String path) throws FileNotFoundException {
        File file = new File(path);
        ArrayList<Item> items = new ArrayList<>();
        if (file.exists()){
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                Item item = new Item();
                String [] itemByFile = scanner.nextLine().split("=");
                item.setName(itemByFile[0]);
                item.setWeight(Integer.parseInt(itemByFile[1]));
                items.add(item);
            }
            return items;
        }else {
            Utils.showMessage("The file don't exist");
        }
        return null;
    }

    public ArrayList<Rocket> loadU1(ArrayList<Item> items){
        ArrayList<Rocket> rockets = new ArrayList<>();
        Rocket rocket = new Rocket(18,10);
        for (int i =0;i<items.size();i++){
            if(rocket.canCarry(items.get(i))){
                rocket.carry(items.get(i));
            }else {
                rockets.add(rocket);
                rocket = new Rocket(18,10);
                i--;
            }
        }
        return rockets;
    }

    public ArrayList<Rocket> loadU2(ArrayList<Item> items){
        ArrayList<Rocket> rockets = new ArrayList<>();
        Rocket rocket = new Rocket(29,18);
        for (int i =0;i<items.size();i++){
            if(rocket.canCarry(items.get(i))){
                rocket.carry(items.get(i));
            }else {
                rockets.add(rocket);
                rocket = new Rocket(29,18);
                i--;
            }
        }
        return rockets;
    }

    public int runSimulation(ArrayList<Rocket> rockets){
        return 0;
    }
}
