import utils.Utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Main {

    public static void main(String [] args) throws FileNotFoundException {

        Simulation simulation = new Simulation();
        ArrayList<Item> phase1 = simulation.loadItems("phase-1.txt");
        ArrayList<Item> phase2 = simulation.loadItems("phase-2.txt");


        ArrayList<Rocket> rocketsU1 =  simulation.loadU1(phase1);
        rocketsU1.addAll(simulation.loadU1(phase2));

        int budgetU1 = simulation.runSimulation(rocketsU1);

        Utils.showMessage("The total budget for U1 is: "+budgetU1);

        ArrayList<Rocket> rocketsU2 = simulation.loadU2(phase1);
        rocketsU2.addAll(simulation.loadU2(phase2));

        int budgetU2 = simulation.runSimulation(rocketsU2);

        Utils.showMessage("The total budget for U2 is: "+budgetU2);

    }
}
